class AddColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :website, :string
    add_column :users, :profile, :string
    add_column :users, :telephone_number, :string
    add_column :users, :sex, :string
  end
end
