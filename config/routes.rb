Rails.application.routes.draw do

  get 'pages/show'
  get '/home', to: 'pages#home'
  get '/term_of_use', to: 'pages#term_of_use'
  # get '/upload', to: 'microposts#upload'
  
  resources :microposts, only: [:new, :create, :destroy, :show]
  resources :microposts do
    resource :favorites, only: [:create, :destroy]
    resource :comments,  only: [:create, :destroy]
  end
  resources :relationships, only: [:create, :destroy]
  resources :notifications, only: :index
  
  get '/upload', to: 'microposts#new'

  
  devise_for :users
  resources :users, only: [:show]
  resources :users do
    member do
      get :following, :followers
    end
  end
  get 'users/index', to: "users#index"
  
devise_scope :user do
  # Deviseのログイン画面をrootに設定
  root to: "devise/sessions#new"
  get 'profile_edit', to: 'users/registrations#profile_edit', as: 'profile_edit'
  patch 'profile_update', to: 'users/registrations#profile_update', as: 'profile_update'
end
end