class Users::RegistrationsController < Devise::RegistrationsController

  def profile_edit
 
  end
 
  def profile_update
    current_user.assign_attributes(account_update_params)
    if current_user.save
	    redirect_to user_path(current_user.id), notice: 'プロフィールを更新しました'
    else
      render "profile_edit"
    end
  end
  
  protected
  
  def configure_account_update_params
   added_attrs = [:fullname, :username, :website, :profile, :telephone_number,
                  :sex]
   devise_parameter_sanitizer.permit(:account_update, keys: added_attrs)
  end

end