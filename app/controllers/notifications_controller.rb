class NotificationsController < ApplicationController
  def index
    # 通知を送った人のリストを得る...visited_idが自分のidと一致するデータのリストを得る
    @notifications = current_user.passive_notifications
    @notifications.where(checked: false).each do |notification|
      notification.update_attributes(checked: true)
    end
  end
end
