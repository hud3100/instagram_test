class PagesController < ApplicationController
  
  before_action :sign_in_required, only: [:show]
    
  def home
    @user = current_user
    # @micropost = current_user.microposts.build
    @feed_items = current_user.feed
    # @feed_items = Micropost.search(params[:search]) || current_user.feed
    @comment = Comment.new
  end

  def show
    # @user = User.find(params[:id])
  end
end
