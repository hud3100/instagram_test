class CommentsController < ApplicationController
  
  def create
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.new(comment_params)
    @comment.user_id = current_user.id
    @comment.save
    @micropost.create_notification_comment!(current_user, @comment.id)
    redirect_to home_path
  end
  
  def destroy
  end
    

  private
    def comment_params
      params.require(:comment).permit(:body)
    end
end
