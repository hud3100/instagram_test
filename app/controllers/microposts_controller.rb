class MicropostsController < ApplicationController
  before_action :authenticate_user!
  
  def new
    @user = User.find(current_user.id)
    # 空のmicropostをいれる
    @micropost = Micropost.new
  end
  
  #検索結果表示
  def index
    @feed_items = Micropost.search(params[:search])
    @comment = Comment.new
  end
  
  def create
    @user = User.find(current_user.id)
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      redirect_to root_url
    end
  end
  
  def show
    # 処理完了後、写真個別ページへリダイレクトする時や写真個別ページを表示させる時
    @micropost = Micropost.find(params[:id])
    @comments = @micropost.comments
    @comment = Comment.new
  end

  def destroy
    @micropost = Micropost.find(params[:id])
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to root_url
  end
  
  def search
    @searched = Micropost.search(params[:search])
  end
  
  private
  
    def micropost_params
      params.require(:micropost).permit(:content, :picture)
    end
  
    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
